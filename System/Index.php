<?php
namespace System;

class Index {
	
	/**
	
	*/
	function __construct($array){
		//$this->db = new \Database();
		$this->start($array);
	}
	
	/**
	
	*/
	public function start($array){
		//echo '<pre>'; print_r($array); echo '</pre>';
		$aliasController = "\\MVC\\Controller\\".ucfirst($array['alias']);
		$aliasModel = "\\MVC\\Model\\".ucfirst($array['alias']);
		//echo "aliasController = $aliasController<br/>\n";
		$$array['alias'] = new $aliasController($array);
		$$array['alias']->model = new $aliasModel();
		$$array['alias']->model->db = new \Database();
		$render = $$array['alias']->start($array);
		$render['alias'] = $array['alias'];
		$this->render($render);
	}
	
	/**
	
	*/
	public function render($array){
		foreach($array as $key=>$value){
			if(preg_match("/^[a-zA-Z0-9_]*$/", $key)){
				$$key = $value;
			}
		}
		require_once('includes/pages/__template.php');
	}
	
}