<?php

namespace MVC\Controller;

class News {
	
	/**
	
	*/
	public function start($array){
		$array['items']      = $this->model->getNews($array['gets']);
		$array['pagesCount'] = $this->model->getPagesCount($array['gets']);
		return $array;
	}
	
}