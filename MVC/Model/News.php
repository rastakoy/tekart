<?php

namespace MVC\Model;

class News {
	
	/**
	
	*/
	function __construct(){
		//
	}
	
	/**
	
	*/
	public function getNews($array){
		$page   = ($array['page'])?$array['page']:1;
		$onPage = ($array['onPage'])?$array['onPage']:$GLOBALS['onPage'];
		$start  = '0'.($page-1)*$onPage;
		$q = "SELECT * FROM `news` ORDER BY `idate` DESC LIMIT $start,$onPage ";
		return $this->db->query($q);
	}
	
	/**
	
	*/
	public function getPagesCount($array){
		$foo = $this->db->query("SELECT COUNT(1) FROM `news`");
		$onPage = ($array['onPage'])?$array['onPage']:$GLOBALS['onPage'];
		return preg_replace("/\.[0-9]*$/", '', $foo['0']['COUNT(1)']/$onPage)+1;
	}
	
}